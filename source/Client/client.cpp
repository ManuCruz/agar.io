#include "client.h"
#include "../Common/common.h"
#include "../Buffer/Buffer.h"
#include "../Common/message.h"

#include "../u-gine/include/u-gine.h"

bool NO_INIT = true;
bool END = false;

Client USER;
typedef std::map<unsigned short, Client> ClientMap;
ClientMap USERS;
typedef std::map<unsigned short, Food> FoodMap;
FoodMap FOODS;
typedef std::vector<unsigned short> idVector;

SOCKET S_TCP = INVALID_SOCKET;
SOCKET S_UDP = INVALID_SOCKET;
struct sockaddr_in SI_SERVER;
int S_LEN = sizeof(SI_SERVER);

int main(int argc, char **argv) {
  printf("CLIENT\n");

  WORD wVersionRequested;
  WSADATA wsaData;
  int err;

  // Validate the parameters
  if (argc != 2) {
    printf("usage: %s server-name\n", argv[0]);
    return 1;
  }

  /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
  wVersionRequested = MAKEWORD(2, 2);

  err = WSAStartup(wVersionRequested, &wsaData);
  if (err != 0) {
    /* Tell the user that we could not find a usable */
    /* Winsock DLL.                                  */
    printf("FATAL: WSAStartup failed with error: %d\n", err);
    return 1;
  }

  struct addrinfo *result = NULL, *ptr = NULL, hints;

  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  // Resolve the server address and port
  err = getaddrinfo(argv[1], SERVER_PORT_TCP_CAD, &hints, &result);
  if (err != 0) {
    printf("FATAL: getaddrinfo failed with error: %d\n", err);
    WSACleanup();
    return 1;
  }

  // Attempt to connect to an address until one succeeds
  for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
    // Create a SOCKET for connecting to server
    S_TCP = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
    if (S_TCP == INVALID_SOCKET) {
      printf("FATAL: socket failed with error: %ld\n", WSAGetLastError());
      WSACleanup();
      return 1;
    }

    // Connect to server.
    err = connect(S_TCP, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (err == SOCKET_ERROR) {
      printf("FATAL: connect failed with error: %ld\n", WSAGetLastError());
      closesocket(S_TCP);
      S_TCP = INVALID_SOCKET;
      continue;
    }
    break;
  }

  freeaddrinfo(result);

  if (S_TCP == INVALID_SOCKET) {
    printf("FATAL: Unable to connect to server!\n");
    WSACleanup();
    return 1;
  }

  pthread_t tcp_thread;
  if (pthread_create(&tcp_thread, NULL, recvTCPThread, NULL)) {
    printf("FATAL: Error creating TCP thread\n");
    closesocket(S_TCP);
    WSACleanup();
    return 1;
  }

  //UDP
  //create socket
  if ((S_UDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR) {
    printf("socket() failed with error code : %d", WSAGetLastError());
    closesocket(S_UDP);
    closesocket(S_TCP);
    WSACleanup();
    return 1;
  }

  //setup address structure
  memset((char *)&SI_SERVER, 0, S_LEN);
  SI_SERVER.sin_family = AF_INET;
  SI_SERVER.sin_port = htons(SERVER_PORT_UDP);
  SI_SERVER.sin_addr.S_un.S_addr = inet_addr(argv[1]);

  pthread_t udp_thread;
  if (pthread_create(&udp_thread, NULL, recvUDPThread, NULL)) {
    printf("FATAL: Error creating UDP thread\n");
    closesocket(S_UDP);
    closesocket(S_TCP);
    WSACleanup();
    return 1;
  }

  Screen::Instance().Open(WIDTH, HEIGHT, false);

  while (NO_INIT);

  NetMessageInput msgInput;
  pthread_mutex_lock(&MUTEX);
  unsigned short id = USER.getId();
  pthread_mutex_unlock(&MUTEX);
  msgInput.setId(id);
  CBuffer *buf = new CBuffer();

  Screen::Instance().SetTitle("Client agar.io");
  while (Screen::Instance().IsOpened() && !Screen::Instance().KeyPressed(GLFW_KEY_ESC) && !END) {
    //dibujar el estado actual de la partida
    pthread_mutex_lock(&MUTEX);
    //jugadores
    for (ClientMap::iterator it = USERS.begin(); it != USERS.end(); ++it){
      Renderer::Instance().SetColor(it->second.r, it->second.g, it->second.b, 0);
      Renderer::Instance().DrawEllipse(it->second.x, it->second.y, it->second.rad, it->second.rad);
    }
    Renderer::Instance().SetColor(256, 256, 256, 0);
    Renderer::Instance().DrawEllipse(USER.x, USER.y, 3, 3);

    //comida
    for (FoodMap::iterator it = FOODS.begin(); it != FOODS.end(); ++it){
      if (it->second.isActive){
        Renderer::Instance().SetColor(it->second.r, it->second.g, it->second.b, 0);
        Renderer::Instance().DrawEllipse(it->second.x, it->second.y, FOOD_RAD, FOOD_RAD);
      }
    }
    pthread_mutex_unlock(&MUTEX);
   
    //enviar el input
    buf->Clear();
    msgInput.setX(Screen::Instance().GetMouseX());
    msgInput.setY(Screen::Instance().GetMouseY());

    msgInput.serialize(buf);

    if (sendto(S_UDP, (char*)buf->GetBytes(), buf->GetSize(), 0, (struct sockaddr *) &SI_SERVER, S_LEN) == SOCKET_ERROR){
      printf("send() failed with error code : %d", WSAGetLastError());
    }

    // Refrescamos la pantalla
    Screen::Instance().Refresh();

    // limpiamos la pantalla
    Renderer::Instance().Clear();
  }

  delete buf;
  WSACleanup();
  return 0;
}

void *recvTCPThread(void *s){
  CBuffer *buf = new CBuffer();
  //recibir el mensaje inicial y rellenar el struct USER
  buf->Clear();
  int sizeR = RecvFull(S_TCP, buf);
  if (sizeR <= 0){
    printf("the server is disconnected\n");
    END = true;
  }
  else{
    NetMessageConnect msgConnect;
    msgConnect.deserialize(buf);

    pthread_mutex_lock(&MUTEX);
    USER = msgConnect.getUser();
    USERS = msgConnect.getClients();
    FOODS = msgConnect.getFoods();
    pthread_mutex_unlock(&MUTEX);

    NO_INIT = false;

    while (!END){
      //recibir mensajes TCP
      buf->Clear();
      sizeR = RecvFull(S_TCP, buf);
      if (sizeR <= 0){
        printf("the server is disconnected\n");
        END = true;
      }
      else{
        NetMessage msg;
        msg.deserialize(buf);

        switch (msg.getType()){
        case NETMSG_NEWS:{
          NetMessageNews msgNews;
          msgNews.deserialize(buf);

          ClientMap news = msgNews.getNewClients();
          pthread_mutex_lock(&MUTEX);
          USERS.insert(news.begin(), news.end());
          pthread_mutex_unlock(&MUTEX);

          ClientMap olds = msgNews.getOldClients();
          pthread_mutex_lock(&MUTEX);
          for (ClientMap::iterator it = olds.begin(); it != olds.end(); ++it)
            if (USERS.count(it->first) == 1)
              USERS.erase(USERS.find(it->first));
          pthread_mutex_unlock(&MUTEX);

          idVector changedFoods = msgNews.getChangedFoods();
          unsigned short size = changedFoods.size();
          pthread_mutex_lock(&MUTEX);
          for (unsigned short i = 0; i < size; i++)
            FOODS[changedFoods[i]].isActive = !FOODS[changedFoods[i]].isActive;
          pthread_mutex_unlock(&MUTEX); 
        }
          break;
        case NETMSG_DEATH:
          END = true;
          break;
        }
        
      }
    }
  }
  delete buf;
  return NULL;
}

void *recvUDPThread(void *s){
  CBuffer *buf = new CBuffer();
  while (!END){
    //recibir mensajes UDP
    buf->Clear();
    if (recvfrom(S_UDP, (char*)buf->GetBytes(), BUF_SIZE, 0, (struct sockaddr *) &SI_SERVER, &S_LEN) != SOCKET_ERROR){
      NetMessageStatus msgStatus;
      msgStatus.deserialize(buf);

      ClientMap status = msgStatus.getClients();
      unsigned short id;
      float x, y, rad;
      for (ClientMap::iterator it = status.begin(); it != status.end(); ++it){
        id = it->first;
        x = it->second.x;
        y = it->second.y;
        rad = it->second.rad;
        pthread_mutex_lock(&MUTEX);
        USERS[id].x = x;
        USERS[id].y = y;
        USERS[id].rad = rad;
        if (USER.getId() == id){
          USER.x = x;
          USER.y = y;
          USER.rad = rad;
        }
        pthread_mutex_unlock(&MUTEX);
      } 
    }
  }

  delete buf;
  return NULL;
}
