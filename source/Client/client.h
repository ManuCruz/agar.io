#ifndef CLIENT_H
#define CLIENT_H

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include <pthread.h>

pthread_mutex_t MUTEX = PTHREAD_MUTEX_INITIALIZER;

void *recvTCPThread(void *s);
void *recvUDPThread(void *s);

#endif
