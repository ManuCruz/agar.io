#include "common.h"
#include <stdio.h>
#include <cstdlib>

int RecvFull(SOCKET s, CBuffer *buf){
  bool cont = true;
  int size = 0;
  char aux[BUF_SIZE];

  //SIMPLIFICADO
  //do{
  //  int r = recv(s, aux, BUF_SIZE, 0);
  //  if (r > 0){
  //    //printf("Recibidos %d bytes\n", r);
  //    size += r;
  //    if (aux[size - 1] == '\0')
  //    cont = false;
  //  }
  //  else{
  //    //conexi�n fallida, liberar el socket
  //    return 0;
  //  }
  //} while (cont);

  size = recv(s, aux, BUF_SIZE, 0);
  if (size <= 0)
    return 0;

  buf->Write(&aux, size);

  return size;
}

int SendFull(SOCKET s, CBuffer *buf){
  int size = buf->GetSize();
  int ret = 0;
  char aux[BUF_SIZE];

  do{
    int r = send(s, (const char*)buf->GetBytes() + ret, size - ret, 0);
    if (r > 0){
      ret += r;
      //printf("Enviados %d bytes\n", r);
    }
    else{
      //conexi�n fallida, liberar el socket
      return 0;
    }
  } while (ret != size);

  return ret;
}

Client::Client(unsigned short id) {
  m_id = id;
  x = (float)(rand() % WIDTH);
  y = (float)(rand() % HEIGHT);
  rad = 10.f;

  isDead = false;

  //se multiplica por 7 en lugar de por 8 para evitar el color negro
  r = (unsigned char)(rand() % 32) * 7 + 8;
  g = (unsigned char)(rand() % 32) * 7 + 8;
  b = (unsigned char)(rand() % 32) * 7 + 8;

  activeUDP = false;
}

Client Client::getObject(){
  Client c(m_id);
  c.x = x;
  c.y = y;
  c.rad = rad;
  c.xTarget = xTarget;
  c.yTarget = yTarget;
  c.speed = speed;
  c.isDead = isDead;
  c.r = r;
  c.g = g;
  c.b = b;
  c.sTCP = sTCP;
  c.activeUDP = activeUDP;
  c.infoUDP = infoUDP;

  return c;
}

Food::Food(unsigned short id){
  m_id = id;
  x = (float)(rand() % WIDTH);
  y = (float)(rand() % HEIGHT);

  //se multiplica por 7 en lugar de por 8 para evitar el color negro
  r = (unsigned char)(rand() % 32) * 7 + 8;
  g = (unsigned char)(rand() % 32) * 7 + 8;
  b = (unsigned char)(rand() % 32) * 7 + 8;

  isActive = true;
}
