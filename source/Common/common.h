#ifndef COMMON_H
#define COMMON_H

#include <winsock2.h>
#include <ws2tcpip.h>
#include "../Buffer/Buffer.h"

#define SERVER_PORT_TCP_CAD "12345" 
#define SERVER_PORT_TCP 12345
#define SERVER_PORT_UDP 12346
#define BUF_SIZE 4096

#define WIDTH 500
#define HEIGHT 500

const float FOOD_RAD = 5.f;

int RecvFull(SOCKET s, CBuffer *buf);
int SendFull(SOCKET s, CBuffer *buf);

class Client{
private:
  unsigned short m_id;
public:
  Client(){}
  Client(unsigned short id);

  unsigned short getId() { return m_id; }
  Client getObject();

  float x;
  float y;
  float rad;

  float xTarget;
  float yTarget;
  float speed;
  bool isDead;

  unsigned char r;
  unsigned char g;
  unsigned char b;

  SOCKET sTCP;
  bool activeUDP;
  struct sockaddr_in infoUDP;
};


class Food{
private:
  unsigned short m_id;
public:
  Food(){}
  Food(unsigned short id);

  unsigned short getId() { return m_id; }

  float x;
  float y;
  unsigned char r;
  unsigned char g;
  unsigned char b;

  bool isActive;
};


#endif