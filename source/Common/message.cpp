#include "message.h"

NetMessageType NetMessage::getType(){
  return Type;
}
void NetMessage::serialize(CBuffer* buf){
  buf->Write(&Type, sizeof(Type));
}
void NetMessage::deserialize(CBuffer* buf){
  buf->GotoStart();
  buf->Read(&Type, sizeof(Type));
}


NetMessageConnect::NetMessageConnect() {
  Type = NETMSG_CONNECT;
}
NetMessageConnect::NetMessageConnect(Client user, ClientMap clients, FoodMap foods){
  Type = NETMSG_CONNECT;
  m_user = user;
  m_clients = clients;
  m_foods = foods;
}
void NetMessageConnect::serialize(CBuffer* buf){
  NetMessage::serialize(buf);
  unsigned short id;
  float x, y, rad;
  unsigned char r, g, b;

  id = m_user.getId();
  buf->Write(&id, sizeof(id));
  x = m_user.x;
  buf->Write(&x, sizeof(x));
  y = m_user.y;
  buf->Write(&y, sizeof(y));
  rad = m_user.rad;
  buf->Write(&rad, sizeof(rad));
  r = m_user.r;
  buf->Write(&r, sizeof(r));
  g = m_user.g;
  buf->Write(&g, sizeof(g));
  b = m_user.b;
  buf->Write(&b, sizeof(b));

  unsigned short size = m_clients.size();
  buf->Write(&size, sizeof(size));
  for (ClientMap::iterator it = m_clients.begin(); it != m_clients.end(); ++it){
    id = it->second.getId();
    buf->Write(&id, sizeof(id));
    x = it->second.x;
    buf->Write(&x, sizeof(x));
    y = it->second.y;
    buf->Write(&y, sizeof(y));
    rad = it->second.rad;
    buf->Write(&rad, sizeof(rad));
    r = it->second.r;
    buf->Write(&r, sizeof(r));
    g = it->second.g;
    buf->Write(&g, sizeof(g));
    b = it->second.b;
    buf->Write(&b, sizeof(b));
  }

  bool isActive;
  size = m_foods.size();
  buf->Write(&size, sizeof(size));
  for (FoodMap::iterator it = m_foods.begin(); it != m_foods.end(); ++it){
    id = it->second.getId();
    buf->Write(&id, sizeof(id));
    x = it->second.x;
    buf->Write(&x, sizeof(x));
    y = it->second.y;
    buf->Write(&y, sizeof(y));
    r = it->second.r;
    buf->Write(&r, sizeof(r));
    g = it->second.g;
    buf->Write(&g, sizeof(g));
    b = it->second.b;
    buf->Write(&b, sizeof(b));
    isActive = it->second.isActive;
    buf->Write(&isActive, sizeof(isActive));
  }
}
void NetMessageConnect::deserialize(CBuffer* buf){
  NetMessage::deserialize(buf);
  unsigned short id;
  float x, y, rad;
  unsigned char r, g, b;

  buf->Read(&id, sizeof(id));
  buf->Read(&x, sizeof(x));
  buf->Read(&y, sizeof(y));
  buf->Read(&rad, sizeof(rad));
  buf->Read(&r, sizeof(r));
  buf->Read(&g, sizeof(g));
  buf->Read(&b, sizeof(b));

  Client user(id);
  user.x = x;
  user.y = y;
  user.rad = rad;
  user.r = r;
  user.g = g;
  user.b = b;

  m_user = user;

  unsigned short size;
  buf->Read(&size, sizeof(size));
  for (unsigned short i = 0; i < size; i++){
    buf->Read(&id, sizeof(id));
    buf->Read(&x, sizeof(x));
    buf->Read(&y, sizeof(y));
    buf->Read(&rad, sizeof(rad));
    buf->Read(&r, sizeof(r));
    buf->Read(&g, sizeof(g));
    buf->Read(&b, sizeof(b));

    Client c(id);
    c.x = x;
    c.y = y;
    c.rad = rad;
    c.r = r;
    c.g = g;
    c.b = b;

    m_clients[id] = c;
  }

  bool isActive;
  buf->Read(&size, sizeof(size));
  for (unsigned short i = 0; i < size; i++){
    buf->Read(&id, sizeof(id));
    buf->Read(&x, sizeof(x));
    buf->Read(&y, sizeof(y));
    buf->Read(&r, sizeof(r));
    buf->Read(&g, sizeof(g));
    buf->Read(&b, sizeof(b));
    buf->Read(&isActive, sizeof(isActive));

    Food f(id);
    f.x = x;
    f.y = y;
    f.r = r;
    f.g = g;
    f.b = b;
    f.isActive = isActive;

    m_foods[id] = f;
  }
}


NetMessageStatus::NetMessageStatus() {
  Type = NETMSG_STATUS;
}
NetMessageStatus::NetMessageStatus(ClientMap clients) {
  Type = NETMSG_STATUS;
  m_clients = clients;
}
void NetMessageStatus::serialize(CBuffer* buf){
  NetMessage::serialize(buf);
  unsigned short size = m_clients.size();
  buf->Write(&size, sizeof(size));
  unsigned short id;
  float x, y, rad;
  for (ClientMap::iterator it = m_clients.begin(); it != m_clients.end(); ++it){
    id = it->second.getId();
    buf->Write(&id, sizeof(id));
    x = it->second.x;
    buf->Write(&x, sizeof(x));
    y = it->second.y;
    buf->Write(&y, sizeof(y));
    rad = it->second.rad;
    buf->Write(&rad, sizeof(rad));
  }
}
void NetMessageStatus::deserialize(CBuffer* buf){
  NetMessage::deserialize(buf);
  unsigned short size;
  buf->Read(&size, sizeof(size));
  unsigned short id;
  float x, y, rad;
  for (unsigned short i = 0; i < size; i++){
    buf->Read(&id, sizeof(id));
    buf->Read(&x, sizeof(x));
    buf->Read(&y, sizeof(y));
    buf->Read(&rad, sizeof(rad));

    Client c(id);
    c.x = x;
    c.y = y;
    c.rad = rad;

    m_clients[id] = c;
  }
}

NetMessageNews::NetMessageNews() {
  Type = NETMSG_NEWS;
}
NetMessageNews::NetMessageNews(ClientMap newC, ClientMap oldC, idVector changedFoods) {
  Type = NETMSG_NEWS;
  m_newC = newC;
  m_oldC = oldC;
  m_changedFoods = changedFoods;
}
void NetMessageNews::serialize(CBuffer* buf){
  NetMessage::serialize(buf);
  unsigned short sizeNew = m_newC.size();
  buf->Write(&sizeNew, sizeof(sizeNew));
  unsigned short sizeOld = m_oldC.size();
  buf->Write(&sizeOld, sizeof(sizeOld));
  unsigned short id;
  float x, y, rad;
  unsigned char r, g, b;
  for (ClientMap::iterator it = m_newC.begin(); it != m_newC.end(); ++it){
    id = it->second.getId();
    buf->Write(&id, sizeof(id));
    x = it->second.x;
    buf->Write(&x, sizeof(x));
    y = it->second.y;
    buf->Write(&y, sizeof(y));
    rad = it->second.rad;
    buf->Write(&rad, sizeof(rad));
    r = it->second.r;
    buf->Write(&r, sizeof(r));
    g = it->second.g;
    buf->Write(&g, sizeof(g));
    b = it->second.b;
    buf->Write(&b, sizeof(b));
  }
  for (ClientMap::iterator it = m_oldC.begin(); it != m_oldC.end(); ++it){
    id = it->second.getId();
    buf->Write(&id, sizeof(id));
  }
  unsigned short sizeF = m_changedFoods.size();
  buf->Write(&sizeF, sizeof(sizeF));
  for (unsigned short i = 0; i < sizeF; i++){
    id = m_changedFoods[i];
    buf->Write(&id, sizeof(id));
  }
}
void NetMessageNews::deserialize(CBuffer* buf){
  NetMessage::deserialize(buf);
  unsigned short sizeNew, sizeOld;
  buf->Read(&sizeNew, sizeof(sizeNew));
  buf->Read(&sizeOld, sizeof(sizeOld));
  unsigned short id;
  float x, y, rad;
  unsigned char r, g, b;
  for (unsigned short i = 0; i < sizeNew; i++){
    buf->Read(&id, sizeof(id));
    buf->Read(&x, sizeof(x));
    buf->Read(&y, sizeof(y));
    buf->Read(&rad, sizeof(rad));
    buf->Read(&r, sizeof(r));
    buf->Read(&g, sizeof(g));
    buf->Read(&b, sizeof(b));

    Client c(id);
    c.x = x;
    c.y = y;
    c.rad = rad;
    c.r = r;
    c.g = g;
    c.b = b;

    m_newC[id] = c;
  }
  for (unsigned short i = 0; i < sizeOld; i++){
    buf->Read(&id, sizeof(id));

    Client c(id);

    m_oldC[id] = c;
  }

  unsigned short sizeF;
  buf->Read(&sizeF, sizeof(sizeF));
  for (unsigned short i = 0; i < sizeF; i++){
    buf->Read(&id, sizeof(id));
    m_changedFoods.push_back(id);
  }
}


NetMessageInput::NetMessageInput() {
  Type = NETMSG_INPUT;
}
NetMessageInput::NetMessageInput(unsigned short id, float x, float y){
  Type = NETMSG_INPUT;
  m_id = id;
  m_x = x;
  m_y = y;
}
void NetMessageInput::serialize(CBuffer* buf){
  NetMessage::serialize(buf);
  buf->Write(&m_id, sizeof(m_id));
  buf->Write(&m_x, sizeof(m_x));
  buf->Write(&m_y, sizeof(m_y));
}
void NetMessageInput::deserialize(CBuffer* buf){
  NetMessage::deserialize(buf);
  buf->Read(&m_id, sizeof(m_id));
  buf->Read(&m_x, sizeof(m_x));
  buf->Read(&m_y, sizeof(m_y));
}


NetMessageDeath::NetMessageDeath() {
  Type = NETMSG_DEATH;
}
void NetMessageDeath::serialize(CBuffer* buf){
  NetMessage::serialize(buf);
}
void NetMessageDeath::deserialize(CBuffer* buf){
  NetMessage::deserialize(buf);
}