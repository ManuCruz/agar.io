#ifndef MESSAGE_H
#define MESSAGE_H

#include "../Buffer/Buffer.h"
#include "../Common/common.h"

#include <map>
#include <vector>

typedef std::map<unsigned short, Client> ClientMap;
typedef std::map<unsigned short, Food> FoodMap;
typedef std::vector<unsigned short> idVector;

enum NetMessageType{
  NETMSG_CONNECT, //mensaje inicial
  NETMSG_STATUS, //mensaje UDP periódico
  NETMSG_NEWS, //mensaje TCP periódico nuevos/viejos
  NETMSG_INPUT, //input del cliente
  NETMSG_DEATH, //muerte del cliente
};

class NetMessage{
protected:
  NetMessageType Type;
public:
  NetMessageType getType();
  virtual void serialize(CBuffer* buf);
  virtual void deserialize(CBuffer* buf);
};

class NetMessageConnect : public NetMessage{
private:
  Client m_user;
  ClientMap m_clients;
  FoodMap m_foods;
public:
  NetMessageConnect();
  NetMessageConnect(Client user, ClientMap clients, FoodMap foods);

  Client getUser() { return m_user; }
  ClientMap getClients() { return m_clients; }
  FoodMap getFoods() { return m_foods; }

  void serialize(CBuffer* buf);
  void deserialize(CBuffer* buf);
};

class NetMessageStatus : public NetMessage{
private:
  ClientMap m_clients;
public:
  NetMessageStatus();
  NetMessageStatus(ClientMap clients);
  
  ClientMap getClients() { return m_clients; }

  void serialize(CBuffer* buf);
  void deserialize(CBuffer* buf);
};

class NetMessageNews : public NetMessage{
private:
  ClientMap m_newC, m_oldC;
  idVector m_changedFoods;
public:
  NetMessageNews();
  NetMessageNews(ClientMap newC, ClientMap oldC, idVector changedFoods);
  
  ClientMap getNewClients() { return m_newC; }
  ClientMap getOldClients() { return m_oldC; }
  idVector getChangedFoods() { return m_changedFoods; }

  void serialize(CBuffer* buf);
  void deserialize(CBuffer* buf);
};

class NetMessageInput : public NetMessage{
private:
  unsigned short m_id;
  float m_x, m_y;
public:
  NetMessageInput();
  NetMessageInput(unsigned short id, float x, float y);

  void setId(unsigned short id) { m_id = id; }
  void setX(float x) { m_x = x; }
  void setY(float y) { m_y = y; }

  unsigned short getId() { return m_id; }
  float getX() { return m_x; }
  float getY() { return m_y; }

  void serialize(CBuffer* buf);
  void deserialize(CBuffer* buf);
};

class NetMessageDeath : public NetMessage{
private:
public:
  NetMessageDeath();

  void serialize(CBuffer* buf);
  void deserialize(CBuffer* buf);
};
#endif