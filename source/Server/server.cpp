#include "server.h"
#include "../Common/message.h"



SOCKET UDP_SOCKET = INVALID_SOCKET;

world::world(){
  m_idCount = 0;
  for (unsigned short i = 0; i < NUM_OF_FOOD; i++){
    Food f(i);

    pthread_mutex_lock(&WORLD_MUTEX);
    WORLD.addFood(f);
    pthread_mutex_unlock(&WORLD_MUTEX);
  }
}
world::~world(){
}
unsigned short world::getNextId(){
  while (m_newPlayers.count(m_idCount) == 1 || m_players.count(m_idCount) == 1 || m_oldPlayers.count(m_idCount) == 1){
    m_idCount++;
    if (m_idCount > 65535)
      m_idCount = 0;
  }
  return m_idCount;
}
void world::addNewPlayer(Client c){
  m_newPlayers[c.getId()] = c;
}
void world::addFood(Food f){
  m_foods[f.getId()] = f;
}
ClientMap world::getNewPlayers(){
  return m_newPlayers;
}
ClientMap world::getPlayers(){
  return m_players;
}
ClientMap world::getOldPlayers(){
  return m_oldPlayers;
}
FoodMap world::getFoods(){
  return m_foods;
}
idVector world::getChangedFoods(){
  return m_changedFoods;
}
void world::cleanChangedFoods(){
  m_changedFoods.clear();
}
void world::promoteNewPlayers(){
  m_players.insert(m_newPlayers.begin(), m_newPlayers.end());
  m_newPlayers.clear();
}
void world::removePlayer(unsigned short id){
  m_oldPlayers[id] = m_players.find(id)->second;
  m_players.erase(m_players.find(id));
}
void world::deleteOldPlayers(){
  m_oldPlayers.clear();
}
void world::updateUDPdata(unsigned short id, struct sockaddr_in infoUDP){
  if (m_players.count(id) == 1)
    if(m_players.find(id)->second.activeUDP == false){
      m_players.find(id)->second.activeUDP = true;
      m_players.find(id)->second.infoUDP = infoUDP;
    }
};
void world::setTargetPosition(unsigned short id, float targetX, float targetY){
  if (m_players.count(id) == 1){
    m_players.find(id)->second.xTarget = targetX;
    m_players.find(id)->second.yTarget = targetY;
  }
}
void world::updatePositions(float step){
  for (ClientMap::iterator it = m_players.begin(); it != m_players.end(); ++it){
    float distX = it->second.xTarget - it->second.x;
    float distY = it->second.yTarget - it->second.y;
    float distance = sqrt(distX * distX + distY*distY);

    if (distance <= it->second.speed * step){
      it->second.x = it->second.xTarget;
      it->second.y = it->second.yTarget;
    }
    else{
      float movingX = (abs(distX) < 0.001f) ? 0.f : it->second.speed * distX / distance;
      float movingY = (abs(distY) < 0.001f) ? 0.f : it->second.speed * distY / distance;

      it->second.x += movingX * step;
      it->second.y += movingY * step;
    }
  }
}
float world::calculateSpeed(float rad){
  float cts = 1000.f;
  float ind = 20.f;

  return ind + (cts / rad);
}
void world::computeColisions(){
  for (ClientMap::iterator it_c = m_players.begin(); it_c != m_players.end(); ++it_c){
    if (!it_c->second.isDead){
      float rad_2 = it_c->second.rad * it_c->second.rad;
      //colision con comida
      for (FoodMap::iterator it_f = m_foods.begin(); it_f != m_foods.end(); ++it_f){
        if (it_f->second.isActive){
          float dist_2 = (it_c->second.x - it_f->second.x)*(it_c->second.x - it_f->second.x) + (it_c->second.y - it_f->second.y)*(it_c->second.y - it_f->second.y);
          if (dist_2 < rad_2){ //no se recoge la comida al tocarla, sino al tocar su centro
            it_f->second.isActive = false;
            m_changedFoods.push_back(it_f->second.getId());
            //Al comer, modificar el radio y recalcular la velocidad
            it_c->second.rad += 1.f;
            it_c->second.speed = calculateSpeed(it_c->second.rad);
          }
        }
      }

      //colision con otros users
      for (ClientMap::iterator it_other = m_players.begin(); it_other != m_players.end(); ++it_other){
        if (!it_other->second.isDead){
          if (it_c != it_other){
            if (it_c->second.rad > it_other->second.rad * 1.2f){ //si es un 20% mayor que el otro
              float dist_2 = (it_c->second.x - it_other->second.x)*(it_c->second.x - it_other->second.x) + (it_c->second.y - it_other->second.y)*(it_c->second.y - it_other->second.y);
              if (dist_2 < rad_2){ //no se come al otro al tocarlo, sino al tocar su centro
                //Al comer, modificar el radio y recalcular la velocidad
                it_c->second.rad += it_other->second.rad * 0.2f;  //al comer a otro usuario, se crece un 20% del tama�o que tuviera
                it_c->second.speed = calculateSpeed(it_c->second.rad);

                it_other->second.isDead = true;
              }
            }
          }
        }
      }
    }
  }
  ClientMap players = WORLD.getPlayers();
  for (ClientMap::iterator it_c = players.begin(); it_c != players.end(); ++it_c){
    if (it_c->second.isDead){
      removePlayer(it_c->first);
    }
  }
}
void world::respawnFoods(){
  for (FoodMap::iterator it_f = m_foods.begin(); it_f != m_foods.end(); ++it_f){
    if (!it_f->second.isActive){
      if (rand() % RESPAWN_VALUE == 0){
        it_f->second.isActive = true;
        m_changedFoods.push_back(it_f->second.getId());
      }
    }
  }
}


int main(){
  printf("SERVER\n");

  WORD wVersionRequested;
  WSADATA wsaData;
  int err;

  /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
  wVersionRequested = MAKEWORD(2, 2);

  err = WSAStartup(wVersionRequested, &wsaData);
  if (err != 0) {
    /* Tell the user that we could not find a usable */
    /* Winsock DLL.                                  */
    printf("FATAL: WSAStartup failed with error: %d\n", err);
    return 1;
  }

  struct sockaddr_in addr; /* contiene la direcci�n IP */

  /* Construye la estructura de la direcci�n para enlazar el socket. */
  memset(&addr, 0, sizeof(addr)); /* canal cero */
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(SERVER_PORT_TCP);

  /* Apertura pasiva. Espera una conexi�n. */
  SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* crea el socket */
  if (s == INVALID_SOCKET){
    printf("FATAL: socket failed");
    WSACleanup();
    return 1;
  }
  int on = 1;
  setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
  int b = bind(s, (struct sockaddr *) &addr, sizeof(addr));
  if (b < 0){
    printf("FATAL: bind failed");
    WSACleanup();
    return 1;
  }
  int l = listen(s, QUEUE_SIZE); /* especifica el tama�o de la cola */
  if (l < 0){
    printf("FATAL: listen failed");
    WSACleanup();
    return 1;
  }

  //hilo comunicationInputThread
  struct sockaddr_in serverUDP;
  if ((UDP_SOCKET = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET) {
    printf("Could not create UDP socket: %d", WSAGetLastError());
    WSACleanup();
    return 1;
  }

  //Prepare the sockaddr_in structure
  serverUDP.sin_family = AF_INET;
  serverUDP.sin_addr.s_addr = INADDR_ANY;
  serverUDP.sin_port = htons(SERVER_PORT_UDP);

  //Bind
  if (bind(UDP_SOCKET, (struct sockaddr *)&serverUDP, sizeof(serverUDP)) == SOCKET_ERROR) {
    printf("UDP Bind failed with error code : %d", WSAGetLastError());
    WSACleanup();
    return 1;
  }

  pthread_t pt_input;
  if (pthread_create(&pt_input, NULL, comunicationInputThread, NULL)) {
    printf("FATAL: Error creating comunicationInput Thread\n");
    WSACleanup();
    return 1;
  }

  //hilo comunicationSendThread
  pthread_t pt_send;
  if (pthread_create(&pt_send, NULL, comunicationSendThread, NULL)) {
    printf("FATAL: Error creating comunicationSend Thread\n");
    WSACleanup();
    return 1;
  }

  //hilo computeThread
  pthread_t pt_compute;
  if (pthread_create(&pt_compute, NULL, computeThread, NULL)) {
    printf("FATAL: Error creating compute Thread\n");
    WSACleanup();
    return 1;
  }

  /* El socket ahora est� configurado y enlazado. Espera una conexi�n y la procesa. */
  while (1) {
    struct sockaddr_in addrClient;
    int sizeClient = sizeof(addrClient);
    memset(&addrClient, 0, sizeClient);

    SOCKET sa = accept(s, (struct sockaddr*) &addrClient, &sizeClient); /* se bloquea para la solicitud de conexi�n */
    if (sa == INVALID_SOCKET){
      printf("FATAL: accept failed: %d\n", WSAGetLastError());
      continue;
    }

    pthread_mutex_lock(&WORLD_MUTEX);
    unsigned short id = WORLD.getNextId();
    pthread_mutex_unlock(&WORLD_MUTEX);

    Client c(id);

    c.sTCP = sa;

    pthread_t pt;
    if (pthread_create(&pt, NULL, clientThread, &c)) {
      printf("FATAL: Error creating client thread\n");
      closesocket(sa);
      continue;
    }

    pthread_mutex_lock(&WORLD_MUTEX);
    c.speed = WORLD.calculateSpeed(c.rad);
    WORLD.addNewPlayer(c);
    pthread_mutex_unlock(&WORLD_MUTEX);
  }

  closesocket(s);
  WSACleanup();
  return 0;
}

void *comunicationInputThread(void *c){
  CBuffer *buf = new CBuffer();
  while (1){
    //recv input de los clientes por udp
    //analizar de que cliente es el mensaje
    buf->Clear();

    struct sockaddr_in si_other;
    int slen = sizeof(si_other);
    if (recvfrom(UDP_SOCKET, (char*)buf->GetBytes(), BUF_SIZE, 0, (struct sockaddr *) &si_other, &slen) != SOCKET_ERROR){
      NetMessageInput msgInput;
      msgInput.deserialize(buf);
      unsigned short id = msgInput.getId();
      float x = msgInput.getX();
      float y = msgInput.getY();

      pthread_mutex_lock(&WORLD_MUTEX);
      WORLD.updateUDPdata(id, si_other);
      WORLD.setTargetPosition(id, x, y);
      pthread_mutex_unlock(&WORLD_MUTEX);
    }
  }

  delete buf;
  return NULL;
}

void SendStatus(CBuffer *buf) {
  //preparar paquete udp
  //para cada uno de los clientes
  //si se tiene su informaci�n udp, se produce el envio
  ClientMap players;
  pthread_mutex_lock(&WORLD_MUTEX);
  players = WORLD.getPlayers();
  pthread_mutex_unlock(&WORLD_MUTEX);

  buf->Clear();

  NetMessageStatus msgStatus(players);

  msgStatus.serialize(buf);

  //para cada cliente
  for (ClientMap::iterator it = players.begin(); it != players.end(); ++it)
    if (it->second.activeUDP){
      if (sendto(UDP_SOCKET, (char*)buf->GetBytes(), buf->GetSize(), 0, (struct sockaddr *) &it->second.infoUDP, sizeof(it->second.infoUDP)) == SOCKET_ERROR) {
        printf("sendto() failed with error code : %d", WSAGetLastError());
        //eliminar
        pthread_mutex_lock(&WORLD_MUTEX);
        WORLD.removePlayer(it->first);
        pthread_mutex_unlock(&WORLD_MUTEX);
      }
    }
}
void SendNews(CBuffer *buf){
  ClientMap newPlayers;
  pthread_mutex_lock(&WORLD_MUTEX);
  newPlayers = WORLD.getNewPlayers();
  WORLD.promoteNewPlayers();
  pthread_mutex_unlock(&WORLD_MUTEX);

  ClientMap oldPlayers;
  pthread_mutex_lock(&WORLD_MUTEX);
  oldPlayers = WORLD.getOldPlayers();
  WORLD.deleteOldPlayers();
  pthread_mutex_unlock(&WORLD_MUTEX);

  idVector changedFoods;
  pthread_mutex_lock(&WORLD_MUTEX);
  changedFoods = WORLD.getChangedFoods();
  WORLD.cleanChangedFoods();
  pthread_mutex_unlock(&WORLD_MUTEX);

  buf->Clear();

  NetMessageNews msgNews(newPlayers, oldPlayers, changedFoods);

  msgNews.serialize(buf);

  //para cada cliente
  ClientMap players;
  pthread_mutex_lock(&WORLD_MUTEX);
  players = WORLD.getPlayers();
  pthread_mutex_unlock(&WORLD_MUTEX);
  for (ClientMap::iterator it = players.begin(); it != players.end(); ++it){
    int sizeS = SendFull(it->second.sTCP, buf);

    if (sizeS <= 0){
      //eliminar
      pthread_mutex_lock(&WORLD_MUTEX);
      WORLD.removePlayer(it->first);
      pthread_mutex_unlock(&WORLD_MUTEX);
    }
  }

  if (oldPlayers.size() > 0){
    buf->Clear();

    NetMessageDeath msgDeath;

    msgDeath.serialize(buf);

    for (ClientMap::iterator it = oldPlayers.begin(); it != oldPlayers.end(); ++it)
      //No hace falta comprobar la desconexi�n, ya que el clinete ya esta en la lista de clientes old
      if (it->second.isDead)  //si no esta muerto, no mandar el mensaje, ya que estara en la lista de old debido a una desconexion
        SendFull(it->second.sTCP, buf);  
  }
}

void *comunicationSendThread(void *c){
  CBuffer *bufUDP = new CBuffer();
  CBuffer *bufTCP = new CBuffer();
  while (1){
    //send udp de datos efimeros a todos
    //send tcp de eventos a todos
    Sleep(50);

    SendStatus(bufUDP);
    
    Sleep(50);

    SendStatus(bufUDP);
    SendNews(bufTCP); 
  }

  delete bufUDP;
  delete bufTCP;

  return NULL;
}

void *clientThread(void *c){
  //ENVIO DE DATOS INICIALES
  Client cl;
  ClientMap players;
  FoodMap foods;
  pthread_mutex_lock(&WORLD_MUTEX);
  cl = *((Client*)c);
  players = WORLD.getPlayers();
  foods = WORLD.getFoods();
  pthread_mutex_unlock(&WORLD_MUTEX);

  CBuffer *buf = new CBuffer();
  buf->Clear();

  NetMessageConnect msgConnet(cl, players, foods);

  msgConnet.serialize(buf);

  int sizeS = SendFull(cl.sTCP, buf);

  if (sizeS <= 0){
    //eliminar
    pthread_mutex_lock(&WORLD_MUTEX);
    WORLD.removePlayer(cl.getId());
    pthread_mutex_unlock(&WORLD_MUTEX);
  }


  //RECV TCP EN ESPERA DE COMANDOS U ORDENES ESPECIALES, ej. DISPAROS
  //NO IMPLEMENTADO

  getchar();
  getchar();

  delete buf;

  return NULL;
}

#include <time.h>
void *computeThread(void *c){
  float step_mseg = 1000.f / 60.f; //l�gica del juego a 60 frames
  float step_seg = 1.f / 60.f; //l�gica del juego a 60 frames
  time_t t1, t2;
  float msecs;
  while (1){
    time(&t1);
    pthread_mutex_lock(&WORLD_MUTEX);
    WORLD.updatePositions(step_seg);
    WORLD.computeColisions();
    WORLD.respawnFoods();
    pthread_mutex_unlock(&WORLD_MUTEX);

    time(&t2);
    msecs = (((float)t2 - (float)t1) / 1000000.f);

    Sleep(step_mseg - msecs);
  }

  return NULL;
}
