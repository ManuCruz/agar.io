#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include <pthread.h>
#include "../Buffer/Buffer.h"
#include "../Common/common.h"

#include <vector>
#include <map>

#define QUEUE_SIZE 10
#define NUM_OF_FOOD 15
#define RESPAWN_VALUE 5000

pthread_mutex_t WORLD_MUTEX = PTHREAD_MUTEX_INITIALIZER;

typedef std::map<unsigned short, Client> ClientMap;
typedef std::map<unsigned short, Food> FoodMap;
typedef std::vector<unsigned short> idVector;

class world{
public:
  world();
  ~world();
  unsigned short getNextId();
  void addNewPlayer(Client c);
  void addFood(Food f);
  ClientMap getNewPlayers();
  ClientMap getPlayers();
  ClientMap getOldPlayers();
  FoodMap getFoods();
  idVector getChangedFoods();
  void cleanChangedFoods();
  void promoteNewPlayers();
  void removePlayer(unsigned short id);
  void deleteOldPlayers();
  void updateUDPdata(unsigned short id, struct sockaddr_in infoUDP);
  void setTargetPosition(unsigned short id, float targetX, float targetY);
  void updatePositions(float step);
  float calculateSpeed(float rad);
  void computeColisions();
  void respawnFoods();

private:
  unsigned short m_idCount;

  ClientMap m_players;
  ClientMap m_newPlayers;
  ClientMap m_oldPlayers;
  FoodMap m_foods;
  idVector m_changedFoods;
}WORLD;


void SendStatus(CBuffer *buf);
void SendNews(CBuffer *buf);


void *comunicationSendThread(void *c);
void *comunicationInputThread(void *c);
void *clientThread(void *c);
void *computeThread(void *c);

#endif
