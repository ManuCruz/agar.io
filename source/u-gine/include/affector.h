#ifndef UGINE_AFFECTOR_H
#define UGINE_AFFECTOR_H

#include "types.h"

class Particle;

class Affector {
public:
  Affector();
  Affector(double x0, double y0, double x1, double y1);
  virtual ~Affector() {}

  virtual void SetVelocityX(double minvelx, double maxvelx);
  virtual void SetVelocityY(double minvely, double maxvely);
  virtual void SetAngularVelocity(double minangvel, double maxangvel);
  virtual void SetMinColor(uint8 r, uint8 g, uint8 b);
  virtual void SetMaxColor(uint8 r, uint8 g, uint8 b);

  virtual bool affect(Particle * particle);

private:
  double x0, y0;
  double x1, y1;

  double minvelx, rangevelx;
  double minvely, rangevely;
  double minangvel, rangeangvel;
  uint8 minr, ming, minb;
  uint8 ranger, rangeg, rangeb;

  bool affectVelx, affectVely, affectAngvel, affectMincolor, affectMaxcolor;
};

#endif
