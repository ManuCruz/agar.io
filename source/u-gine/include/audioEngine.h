#ifndef UGINE_AUDIO_ENGINE_H
#define UGINE_AUDIO_ENGINE_H

#include <stdio.h>

class AudioEngine {
public:
  static AudioEngine& Instance();

  void Init();
  void Finish();

  void SetDopplerFactor(float factor);

protected:
  AudioEngine() : device(NULL), context(NULL) {}
  ~AudioEngine();

private:
  static AudioEngine* engine;
  void* device;
  void* context;
};

#endif
