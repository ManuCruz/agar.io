#ifndef UGINE_EMITTER_H
#define UGINE_EMITTER_H

#include "types.h"
#include "renderer.h"
#include "particle.h"
#include "affector.h"

class Image;

class Emitter {
public:
  Emitter(Image* image, bool autofade);
  virtual ~Emitter(){};
  virtual void SetPosition(double x, double y) { this->x = x; this->y = y; }
  virtual void SetX(double x) { this->x = x; }
  virtual void SetY(double y) { this->y = y; }
  virtual double GetX() const { return x; }
  virtual double GetY() const { return y; }
  virtual void SetRate(double minrate, double maxrate);
  virtual void SetVelocityX(double minvelx, double maxvelx);
  virtual void SetVelocityY(double minvely, double maxvely);
  virtual void SetAngularVelocity(double minangvel, double maxangvel);
  virtual void SetLifetime(double minlifetime, double maxlifetime);
  virtual void SetMinColor(uint8 r, uint8 g, uint8 b);
  virtual void SetMaxColor(uint8 r, uint8 g, uint8 b);
  virtual void SetBlendMode(Renderer::BlendMode mode) { blendMode = mode; }
  //square shape: shape0 = width, shape1 = height
  //circular shape: shape0 = radius, shape1 = 0
  virtual void SetShape(double shape0, double shape1 = 0.);
  virtual void Start();
  virtual void Stop();
  virtual bool IsEmitting() const { return emitting; }
  virtual void Update(double elapsed);
  virtual void Render() const;

  virtual void SetAffector(Affector aff) { affectors.Add(aff); }

private:
  Image* image;
  bool autofade;
  double x, y;
  double shape0, shape1;
  double minrate, rangerate;
  double emitionRemains;
  double minvelx, rangevelx;
  double minvely, rangevely;
  double minangvel, rangeangvel;
  double minlifetime, rangelifetime;
  uint8 minr, ming, minb;
  uint8 ranger, rangeg, rangeb;
  bool hasShape;
  Renderer::BlendMode blendMode;
  bool emitting;
  Array<Particle> particles;
  Array<Affector> affectors;
};

#endif