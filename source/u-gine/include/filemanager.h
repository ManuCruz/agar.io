#ifndef UGINE_FILEMANAGER_H
#define UGINE_FILEMANAGER_H

#include "array.h"
#include "string.h"
#include "types.h"

class PakFile;

class FileManager {
public:
  static FileManager& Instance();

  bool AddPak(const String& filename);

  bool FileInPak(const String& filename) const { return PakForFile(filename) != NULL; }
  uint32 FileSize(const String& filename) const;
  bool GetFile(const String& filename, void* buffer) const;
  String LoadString(const String& filename) const;

protected:
  FileManager() {}
  virtual ~FileManager();
  const PakFile* PakForFile(const String& filename) const;

private:
  static FileManager* mInstance;
  Array<PakFile*> mPakFiles;
};

#endif
