#ifndef UGINE_GLYPH_H
#define UGINE_GLYPH_H

#include "types.h"

class Glyph{
public:
  Glyph() { x = 0; y = 0; tamX = 0; tamY = 0; offsetY = 0; }
  virtual ~Glyph() {}

  virtual uint16 getX() const { return x; }
  virtual uint16 getY() const { return y; }
  virtual void setX(uint16 paramX) { x = paramX; }
  virtual void setY(uint16 paramY) { y = paramY; }
  virtual uint16 getTamX() const { return tamX; }
  virtual uint16 getTamY() const { return tamY; }
  virtual void setTamX(uint16 paramX) { tamX = paramX; }
  virtual void setTamY(uint16 paramY) { tamY = paramY; }
  virtual float getOffsetY() const { return offsetY; }
  virtual void setOffsetY(float yoffset) { offsetY = yoffset; }
private:
  uint16 x, y;
  uint16 tamX, tamY;
  float offsetY;
};

#endif