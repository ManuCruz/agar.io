#ifndef UGINE_ISOMETRICSPRITE_H
#define UGINE_ISOMETRICSPRITE_H

#include "sprite.h"
#include "types.h"

class Image;
class Map;

class IsometricSprite : public Sprite {
public:
  IsometricSprite(Image* image);
  virtual ~IsometricSprite() {}
  virtual void SetPosition(double x, double y, double z) { Sprite::SetPosition(x, y);  this->z = z; }
  virtual void SetZ(double z) { this->z = z; }
  virtual double GetZ() const { return z; }

  virtual double GetScreenX() const { return screenX; }
  virtual double GetScreenY() const { return screenY; }

  virtual void SetCollision(CollisionMode mode);
  virtual void SetCollisionSize(double colW = 0., double colH = 0.);

  virtual void Update(double elapsed, const Map* map = NULL);

protected:
  virtual void UpdateCollisionBox();

private:
  double z;
  double screenX, screenY;

  bool customColSize;
  double customColW, customColH;
};

#endif
