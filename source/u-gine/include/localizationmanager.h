#ifndef UGINE_LOCALIZATIONMANAGER_H
#define UGINE_LOCALIZATIONMANAGER_H

#include "array.h"
#include "string.h"
#include "types.h"

class LangDef {
public:
  LangDef() {}
  LangDef(const String& name) { mName = name; }
  const String& GetName() const { return mName; }
  void AddString(const String& key, const String& value) { mKeys.Add(key); mValues.Add(value); }
  const String& GetString(const String& key, const String& defVal) const;

private:
  String mName;
  Array<String> mKeys;
  Array<String> mValues;
};

class LocalizationManager {
public:
  static LocalizationManager& Instance();

  bool ParseLanguage(const String& filename);
  bool SetLanguage(const String& name);
  const String& GetCurrentLanguage() const { return mLanguages[mCurrentLanguage].GetName(); }
  const String& GetString(const String& key, const String& defVal = "") const { return mLanguages[mCurrentLanguage].GetString(key, defVal); }

private:
  static LocalizationManager* instance;
  Array<LangDef> mLanguages;
  uint32 mCurrentLanguage;
};

inline const String& LangDef::GetString(const String& key, const String& defVal) const {
  for ( uint32 i = 0; i < mKeys.Size(); i++ ) {
    if ( mKeys[i] == key ) return mValues[i];
  }
  return defVal;
}

inline bool LocalizationManager::SetLanguage(const String& name) {
  for ( uint32 i = 0; i < mLanguages.Size(); i++ ) {
    if ( mLanguages[i].GetName() == name ) {
      mCurrentLanguage = i;
      return true;
    }
  }
  return false;
}

#endif
