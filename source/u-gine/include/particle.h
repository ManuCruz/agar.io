#ifndef UGINE_PARTICLE_H
#define UGINE_PARTICLE_H

#include "types.h"
#include "sprite.h"

class Image;

class Particle : public Sprite {
public:
  Particle();
  Particle(Image* image, double velx, double vely, double angularVel, double lifetime, bool autofade);
  virtual ~Particle() {}

  virtual double GetLifetime() const { return lifetime; }
  virtual void Update(double elapsed);

  virtual void SetAffected(bool aff) { affected = aff; }
  virtual bool IsAffected() { return affected; }
  virtual void SetVelX(double velX) { velocityx = velX; }
  virtual void SetVelY(double velY) { velocityy = velY; }
  virtual void SetAngVel(double angVel) { angularVelocity = angVel; }

private:
  double velocityx, velocityy;
  double angularVelocity;
  double lifetime;
  double initialLifetime;
  uint8 initialAlpha;
  bool autofade;
  bool affected;
};

#endif