#ifndef UGINE_PIXELCOLLISON_H
#define UGINE_PIXELCOLLISON_H

#include "collision.h"
#include "collisionmanager.h"

class PixelCollision : public Collision {
public:
  PixelCollision(double* x, double* y, double* width, double* height, const CollisionPixelData* pixels) : x(x), y(y), width(width), height(height), pixels(pixels){}
  virtual ~PixelCollision() {}

  virtual bool DoesCollide(const Collision* other) const { return other->DoesCollide(pixels, *x, *y); }
  virtual bool DoesCollide(double cx, double cy, double cradius) const { return CollisionManager::Instance().CircleToPixels(cx, cy, cradius, pixels, *x, *y); }
  virtual bool DoesCollide(double rx, double ry, double rwidth, double rheight) const { return CollisionManager::Instance().PixelsToRect(pixels, *x, *y, rx, ry, rwidth, rheight); }
  virtual bool DoesCollide(const CollisionPixelData* pixels, double px, double py) const { return CollisionManager::Instance().PixelsToPixels(pixels, px, py, this->pixels, *x, *y); }

private:
  double* x;
  double* y;
  double* width;
  double* height;
  const CollisionPixelData* pixels;
};


#endif
