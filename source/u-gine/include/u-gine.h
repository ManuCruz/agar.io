#ifndef UGINE_MAIN_H
#define UGINE_MAIN_H

#include "affector.h"
#include "array.h"
#include "bone.h"
#include "camera.h"
#include "collisionpixeldata.h"
#include "emitter.h"
#include "filemanager.h"
#include "font.h"
#include "glinclude.h"
#include "image.h"
#include "isometricmap.h"
#include "isometricscene.h"
#include "isometricsprite.h"
#include "localizationmanager.h"
#include "map.h"
#include "mapscene.h"
#include "math.h"
#include "parallaxscene.h"
#include "particle.h"
#include "renderer.h"
#include "resourcemanager.h"
#include "scene.h"
#include "screen.h"
#include "skeletonsprite.h"
#include "sprite.h"
#include "string.h"
#include "types.h"

#include "audioEngine.h"
#include "audioBuffer.h"
#include "audioSource.h"
#include "audioStream.h"
#include "listener.h"

#include <math.h>

#endif
