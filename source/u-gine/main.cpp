#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include "include/u-gine.h"

#define SCR Screen::Instance()
#define REN Renderer::Instance()
#define RES ResourceManager::Instance()
#define AUD AudioEngine::Instance()
#define LIS Listener::Instance()

const enum practicas{
  prac1,
  prac2,
  prac3,
};

void practica1();
void practica2();
void practica3();

void main() {
  practicas practica = prac3;

  switch (practica) {
  case prac1:
    practica1();
    break;
  case prac2:
    practica2();
    break;
  case prac3:
    practica3();
    break;
  
  default:
    break;
  }
}

void practica3(){
  AUD.Init();
  SCR.Open(500, 500, false);

  //AudioBuffer *aBuffer = RES.LoadAudioBuffer("data/mutant.ogg");
  //AudioSource *aSource = new AudioSource(aBuffer);

  AudioSource *aSource = new AudioSource("data/mutant.ogg");


  aSource->Play();

  while (SCR.IsOpened() && !SCR.KeyPressed(GLFW_KEY_ESC) && aSource->IsPlaying()) {
    // limpiamos la pantalla
    REN.Clear();

    AudioStream::UpdateAll();

    // Refrescamos la pantalla
    SCR.Refresh();
  }

  delete aSource;
  RES.FreeResources();
  AUD.Finish();
}

void practica2(){
  SCR.Open(600, 600, false);

  AUD.Init();

  AudioBuffer *aBuffer = RES.LoadAudioBuffer("data/music.wav");
  AudioSource *aSource = new AudioSource(aBuffer);

  aSource->Play();

  double screenW = SCR.GetWidth();
  double screenH = SCR.GetHeight();

  double xCenter = screenW / 2;
  double yCenter = screenH / 2;

  double xEllipse = xCenter, yEllipse = yCenter;
  double xEllipsePrev, yEllipsePrev;
  uint8 tamEllipse = 25;
  double countEllipse = 0.;
  uint8 radiusTranslationEllipse = screenH/4;
  double timeTranslationEllipse = 64;

  double xOyente = screenW/2;
  double yOyente = screenH*0.9;
  double minX = 0.;
  double maxX = screenW;
  double step = 10.;

  float dopplerFactor = 1.f;
  float stepDopplerFactor = 0.01f;

  while (SCR.IsOpened() && !SCR.KeyPressed(GLFW_KEY_ESC)) {
    // limpiamos la pantalla
    REN.Clear();

    if (SCR.KeyPressed(GLFW_KEY_RIGHT) && xOyente < maxX)
      xOyente += step;
    if (SCR.KeyPressed(GLFW_KEY_LEFT) && xOyente > minX)
      xOyente -= step;

    LIS.SetPosition(xOyente, yOyente, 0.);

    if (SCR.KeyPressed(GLFW_KEY_UP) && xOyente < maxX)
      dopplerFactor += stepDopplerFactor;
    if (SCR.KeyPressed(GLFW_KEY_DOWN) && dopplerFactor > 0){
      dopplerFactor -= stepDopplerFactor;
      if (dopplerFactor < 0)
        dopplerFactor = 0;
    }

    xEllipsePrev = xEllipse;
    yEllipsePrev = yEllipse;
    xEllipse = xCenter + DegCos(countEllipse) * radiusTranslationEllipse;
    yEllipse = yCenter + DegSin(countEllipse) * radiusTranslationEllipse;
    countEllipse += SCR.ElapsedTime() * timeTranslationEllipse;

    aSource->SetPosition(xEllipse, yEllipse, 0);
    aSource->SetVelocity(xEllipse - xEllipsePrev, yEllipse - yEllipsePrev, 0);

    REN.SetColor(255, 0, 0, 0);
    REN.DrawEllipse(xEllipse, yEllipse, tamEllipse, tamEllipse);

    REN.SetColor(0, 255, 0, 0);
    REN.DrawEllipse(xOyente, yOyente, tamEllipse, tamEllipse);

    SCR.SetTitle("dopplerFactor: " + String::FromFloat(dopplerFactor));

    // Refrescamos la pantalla
    SCR.Refresh();
  }

  delete aSource;
  RES.FreeResources();
  AUD.Finish();
}

void practica1(){
  AUD.Init();
  LIS.SetPosition(10., 0., 0.);

  AudioBuffer *aBuffer = RES.LoadAudioBuffer("data/music.wav");
  AudioSource *aSource = new AudioSource(aBuffer);

  aSource->Play();

  float pitch = 1.;
  float minPitch = 0.5;
  float maxPitch = 2.;
  float x = 0.;
  float minX = -10.;
  float maxX = 10.;

  float step = 0.1f;

  SCR.Open(500, 500, false);

  while (SCR.IsOpened() && !SCR.KeyPressed(GLFW_KEY_ESC)) {
    // limpiamos la pantalla
    REN.Clear();

    if (SCR.KeyOnce(GLFW_KEY_RIGHT) && x < maxX){
      x += step*10;
      aSource->SetPosition(x, 0., 0.);
    }
    if (SCR.KeyOnce(GLFW_KEY_LEFT) && x > minX){
      x -= step * 10;
      aSource->SetPosition(x, 0., 0.);
    }
    if (SCR.KeyOnce(GLFW_KEY_UP) && pitch < maxPitch){
      pitch += step;
      aSource->SetPitch(pitch);
    }
    if (SCR.KeyOnce(GLFW_KEY_DOWN) && pitch > minPitch){
      pitch -= step;
      aSource->SetPitch(pitch);
    }

    SCR.SetTitle("Pitch: " + String::FromFloat(pitch) + " --- X value: " + String::FromFloat(x));

    // Refrescamos la pantalla
    SCR.Refresh();
  }

  delete aSource;
  RES.FreeResources();
  AUD.Finish();
}
