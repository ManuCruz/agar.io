#include "../include/affector.h"
#include "../include/particle.h"
#include "../include/math.h"

Affector::Affector(){
  x0 = y0 = x1 = y1 = 0.;

  minvelx = rangevelx = 0.;
  minvely = rangevely = 0.;
  minangvel = rangeangvel = 0.;
  minr = ming = minb = 0;
  ranger = rangeg = rangeb = 255;
  affectVelx = affectVely = affectAngvel = affectMincolor = affectMaxcolor = false;
}

Affector::Affector(double x0, double y0, double x1, double y1){
  this->x0 = x0;
  this->y0 = y0;
  this->x1 = x1;
  this->y1 = y1;

  minvelx = rangevelx = 0.;
  minvely = rangevely = 0.;
  minangvel = rangeangvel = 0.;
  minr = ming = minb = 0;
  ranger = rangeg = rangeb = 255;
  affectVelx = affectVely = affectAngvel = affectMincolor = affectMaxcolor = false;
}

void Affector::SetVelocityX(double minvelx, double maxvelx){
#ifdef _DEBUG
  if (minvelx <= maxvelx){
    this->minvelx = minvelx;
    rangevelx = maxvelx - minvelx;
    affectVelx = true;
  }
#else
  this->minvelx = minvelx;
  rangevelx = maxvelx - minvelx;
  affectVelx = true;
#endif
}

void Affector::SetVelocityY(double minvely, double maxvely){
#ifdef _DEBUG
  if (minvely <= maxvely){
    this->minvely = minvely;
    rangevely = maxvely - minvely;
    affectVely = true;
  }
#else
  this->minvely = minvely;
  rangevely = maxvely - minvely;
  affectVely = true;
#endif
}

void Affector::SetAngularVelocity(double minangvel, double maxangvel){
#ifdef _DEBUG
  if (minangvel <= maxangvel){
    this->minangvel = minangvel;
    rangeangvel = maxangvel - minangvel;
    affectAngvel = true;
  }
#else
  this->minangvel = minangvel;
  rangeangvel = maxangvel - minangvel;
  affectAngvel = true;
#endif
}

void Affector::SetMinColor(uint8 r, uint8 g, uint8 b){
#ifdef _DEBUG
  if (r <= ranger && g <= rangeg && b <= rangeb){
    minr = r;
    ming = g;
    minb = b;
    ranger = ranger - minr;
    rangeg = rangeg - ming;
    rangeb = rangeb - minb;
    affectMincolor = true;
  }
#else
  minr = r;
  ming = g;
  minb = b;
  ranger = ranger - minr;
  rangeg = rangeg - ming;
  rangeb = rangeb - minb;
  affectMincolor = true;
#endif
  }

void Affector::SetMaxColor(uint8 r, uint8 g, uint8 b){
#ifdef _DEBUG
  if (minr <= r && ming <= g && minb <= b){
    ranger = r - minr;
    rangeg = g - ming;
    rangeb = b - minb;
    affectMaxcolor = true;
}
#else
  ranger = r - minr;
  rangeg = g - ming;
  rangeb = b - minb;
  affectMaxcolor = true;
#endif
}

bool Affector::affect(Particle *particle){
  if (x0 <= particle->GetX() && particle->GetX() <= x1 && y0 <= particle->GetY() && particle->GetY() <= y1){
    if (affectVelx)
      (rangevelx) ? particle->SetVelX(WrapValue(rand(), rangevelx) + minvelx) : particle->SetVelX(minvelx);
    if (affectVely)
      (rangevely) ? particle->SetVelY(WrapValue(rand(), rangevely) + minvely) : particle->SetVelY(minvely);
    if (affectAngvel)
      (rangeangvel) ? particle->SetAngVel(WrapValue(rand(), rangeangvel) + minangvel) : particle->SetAngVel(minangvel);
    if (affectMincolor || affectMaxcolor){
      uint8 cr = (ranger) ? rand() % ranger + minr : minr;
      uint8 cg = (rangeg) ? rand() % rangeg + ming : ming;
      uint8 cb = (rangeb) ? rand() % rangeb + minb : minb;
      particle->SetColor(cr, cg, cb, particle->GetAlpha());
    }
    return true;
  }
  return false;
}