#include "../include/audioBuffer.h"
#include "../include/file.h"
#include "../lib/AL/al.h"

#define STB_VORBIS_HEADER_ONLY
extern "C"{
  #include "../lib/stb_vorbis.c"
}

AudioBuffer::AudioBuffer(const String& filename){
  alBuffer = 0;

  if (filename.ExtractExt().Lower() == String("wav")){
    LoadWav(filename);
  }
  else if (filename.ExtractExt().Lower() == String("ogg")){
    LoadOgg(filename);
  }
}

void AudioBuffer::LoadWav(const String& filename){
  File f(filename, FILE_READ);

  if (f.IsOpened()){
    char buffer[5];
    f.ReadBytes(buffer, 4);
    buffer[4] = '\0';
    if (String(buffer) == String("RIFF")){
      f.ReadInt(); //RiffChunkSize
      f.ReadBytes(buffer, 4);
      buffer[4] = '\0';
      if (String(buffer) == String("WAVE")){
        f.ReadBytes(buffer, 4);
        buffer[4] = '\0';
        if (String(buffer) == String("fmt ")){
          uint32 FmtChunkSize = f.ReadInt();
          f.ReadInt16(); //AudioFormat
          uint16 Channels = f.ReadInt16();
          uint32 SampleRate = f.ReadInt();
          f.ReadInt(); //ByteRate
          f.ReadInt16(); //BlockAlign
          uint16 BitsPerSample = f.ReadInt16();
          if (FmtChunkSize > 16){
            uint16 ExtraParamsSize = f.ReadInt16();
            f.Seek(f.Pos() + ExtraParamsSize);
          }

          while (String(buffer) != String("data")){
            f.ReadBytes(buffer, 4);
            buffer[4] = '\0';
            if (String(buffer) != String("data")){
              uint32 size = f.ReadInt();
              f.Seek(f.Pos() + size);
            }
          }

          uint32 sizeData = f.ReadInt();
          uint8 *data = (uint8 *)malloc(sizeData);
          f.ReadBytes(data, sizeData);

          alGenBuffers(1, &alBuffer);
          if (BitsPerSample == 8){
            alBufferData(alBuffer, (Channels == 1) ? AL_FORMAT_MONO8 : AL_FORMAT_STEREO8, data, sizeData, SampleRate);
          }
          else if (BitsPerSample == 16){
            alBufferData(alBuffer, (Channels == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16, data, sizeData, SampleRate);
          }

          free(data);
        }
      }
    }
    f.Close();
  }
}

void AudioBuffer::LoadOgg(const String& filename){
  stb_vorbis *pVorbis = stb_vorbis_open_filename(filename.ToCString(), NULL, NULL);

  if (pVorbis){
    stb_vorbis_info info = stb_vorbis_get_info(pVorbis);
    int lenght = info.channels * stb_vorbis_stream_length_in_samples(pVorbis);
    short *buffer = new short[lenght];
    stb_vorbis_get_samples_short_interleaved(pVorbis, info.channels, buffer, lenght);
    alGenBuffers(1, &alBuffer);
    alBufferData(alBuffer, AL_FORMAT_STEREO16, buffer, lenght, info.sample_rate);
    stb_vorbis_close(pVorbis);

    delete buffer;
  }

}


AudioBuffer::~AudioBuffer(){
  alDeleteBuffers(1, &alBuffer);
}