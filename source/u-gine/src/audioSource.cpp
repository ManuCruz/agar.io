#include "../include/audioSource.h"
#include "../include/audioBuffer.h"
#include "../include/audioStream.h"
#include "../lib/AL/al.h"

AudioSource::AudioSource(AudioBuffer* buffer){
  this->buffer = buffer;
  stream = NULL;

  alGenSources(1, &source);

  SetLooping(false);
  SetPitch(1);
  SetGain(1);
  SetPosition(0, 0, 0);
  SetVelocity(0, 0, 0);

  alSourcei(source, AL_BUFFER, buffer->GetBuffer());
}

AudioSource::AudioSource(const String& filename){
  alGenSources(1, &source);

  SetLooping(false);
  SetPitch(1);
  SetGain(1);
  SetPosition(0, 0, 0);
  SetVelocity(0, 0, 0);

  buffer = NULL;
  stream = new AudioStream(filename, this);
}

AudioSource::~AudioSource(){
  alDeleteSources(1, &source);
  if (stream)
    delete stream;
}

void AudioSource::SetPitch(float pitch){
  alSourcef(source, AL_PITCH, pitch);
}

void AudioSource::SetGain(float gain){
  alSourcef(source, AL_GAIN, gain);
}

void AudioSource::SetLooping(bool loop){
  if (stream)
    stream->SetLooping(loop);
  else
    alSourcei(source, AL_LOOPING, loop);
}

void AudioSource::SetPosition(float x, float y, float z){
  alSource3f(source, AL_POSITION, x, y, z);
}

void AudioSource::SetVelocity(float x, float y, float z){
  alSource3f(source, AL_VELOCITY, x, y, z);
}

void AudioSource::Play(){
  alSourcePlay(source);
}

void AudioSource::Stop(){
  alSourceStop(source);
}

void AudioSource::Pause(){
  alSourcePause(source);
}

bool AudioSource::IsPlaying() const{
  int32 value;
  alGetSourcei(source, AL_SOURCE_STATE, &value);

  return (value == AL_PLAYING) ? true : false;
}