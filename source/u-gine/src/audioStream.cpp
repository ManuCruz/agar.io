#include "../include/audioStream.h"
#include "../include/audioSource.h"
#include "../lib/AL/al.h"

Array<AudioStream*> AudioStream::streams;

AudioStream::AudioStream(const String& filename, AudioSource* source){
  this->source = source;
  stream = stb_vorbis_open_filename(filename.ToCString(), NULL, NULL);
  info = stb_vorbis_get_info(stream);
  samplesLeft = stb_vorbis_stream_length_in_samples(stream) * info.channels;
  alGenBuffers(2, (ALuint*)&buffers);
  Stream(buffers[0]);
  Stream(buffers[1]);

  alSourceQueueBuffers(source->getSource(), 2, buffers);

  streams.Add(this);
}

AudioStream::~AudioStream(){
  streams.Remove((AudioStream*)this);
  alDeleteSources(2, (ALuint*)&buffers);
}

void AudioStream::UpdateAll(){
  int len = streams.Size();
  for (int i = 0; i < len; i++)
    streams[i]->Update();
}

void AudioStream::Update(){
  int value;
  int sourceID = source->getSource();
  alGetSourcei(sourceID, AL_BUFFERS_PROCESSED, &value);
  for (int i = 0; i < value; i++){
    unsigned int buffer;
    alSourceUnqueueBuffers(sourceID, 1, &buffer);
    if(Stream(buffer))
      alSourceQueueBuffers(sourceID, 1, &buffer);
    else{
      stb_vorbis_seek_start(stream);
      samplesLeft = stb_vorbis_stream_length_in_samples(stream) * info.channels;
      if (Stream(buffer))
        alSourceQueueBuffers(sourceID, 1, &buffer);
    }
  }
}

bool AudioStream::Stream(unsigned int buffer){
  short *pcm = new short[32768];

  int size = stb_vorbis_get_samples_short_interleaved(stream, info.channels, pcm, 32768);

  if (size == 0){
    delete pcm;
    return false;
  }
  else{
    alBufferData(buffer, AL_FORMAT_STEREO16, pcm, size*info.channels*sizeof(short), info.sample_rate);
    samplesLeft -= size;
    delete pcm;
    return true;
  }
}
