#include "../include/collisionpixeldata.h"
#include "../include/filemanager.h"
#include "../lib/stb_image.h"

CollisionPixelData::CollisionPixelData(const String& filename) {
  this->filename = filename;
  data = NULL;

  int32 w, h;
  
  uint32 tam = FileManager::Instance().FileSize(filename);
  uint8 *buf = new uint8[tam];
  FileManager::Instance().GetFile(filename, buf);

  uint8 *buffer = stbi_load_from_memory(buf, tam, &w, &h, NULL, 4);

  delete buf;

  if (buffer){
    data = new bool[w*h];
    
    height = static_cast<uint16>(h);
    width = static_cast<uint16>(w);

    for (int32 i = 0; i < height; i++)
      for (int32 j = 0; j < width; j ++)
        data[width*i + j] = (buffer[(width*i + j)*4 + 3]) == 0 ? false : true;

    // liberación del buffer
    stbi_image_free(buffer);
  }
}
