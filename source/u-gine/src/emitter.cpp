#include "../include/emitter.h"
#include "../include/image.h"
#include "../include/math.h"
#include "math.h"

Emitter::Emitter(Image* image, bool autofade){
  this->image = image;
  this->autofade = autofade;
  x = y = 0.;
  shape0 = shape1 = 0.;
  minrate = rangerate = 0.;
  emitionRemains = 0.;
  minvelx = rangevelx = 0.;
  minvely = rangevely = 0.;
  minangvel = rangeangvel = 0.;
  minlifetime = rangelifetime = 0.;
  minr = ming = minb = 0;
  ranger = rangeg = rangeb = 255;
  blendMode = Renderer::ADDITIVE;
  emitting = false;
  hasShape = false;
}

void Emitter::SetRate(double minrate, double maxrate){
#ifdef _DEBUG
  if(minrate <= maxrate){
    this->minrate = minrate;
    rangerate = maxrate - minrate;
  }
#else
  this->minrate = minrate;
  rangerate = maxrate - minrate;
#endif
}

void Emitter::SetVelocityX(double minvelx, double maxvelx){
#ifdef _DEBUG
  if (minvelx <= maxvelx){
    this->minvelx = minvelx;
    rangevelx = maxvelx - minvelx;
  }
#else
  this->minvelx = minvelx;
  rangevelx = maxvelx - minvelx;
#endif
}

void Emitter::SetVelocityY(double minvely, double maxvely){
#ifdef _DEBUG
  if (minvely <= maxvely){
    this->minvely = minvely;
    rangevely = maxvely - minvely;
  }
#else
  this->minvely = minvely;
  rangevely = maxvely - minvely;
#endif
}

void Emitter::SetAngularVelocity(double minangvel, double maxangvel){
#ifdef _DEBUG
  if (minangvel <= maxangvel){
    this->minangvel = minangvel;
    rangeangvel = maxangvel - minangvel;
  }
#else
  this->minangvel = minangvel;
  rangeangvel = maxangvel - minangvel;
#endif
}

void Emitter::SetLifetime(double minlifetime, double maxlifetime){
#ifdef _DEBUG
  if (minlifetime <= maxlifetime){
    this->minlifetime = minlifetime;
    rangelifetime = maxlifetime - minlifetime;
  }
#else
  this->minlifetime = minlifetime;
  rangelifetime = maxlifetime - minlifetime;
#endif
}

void Emitter::SetMinColor(uint8 r, uint8 g, uint8 b){
#ifdef _DEBUG
  if (r <= ranger && g <= rangeg && b <= rangeb){
    minr = r;
    ming = g;
    minb = b;
    ranger = ranger - minr;
    rangeg = rangeg - ming;
    rangeb = rangeb - minb;
  }
#else
  minr = r;
  ming = g;
  minb = b;
  ranger = ranger - minr;
  rangeg = rangeg - ming;
  rangeb = rangeb - minb;
#endif
}

void Emitter::SetMaxColor(uint8 r, uint8 g, uint8 b){
#ifdef _DEBUG
  if (minr <= r && ming <= g && minb <= b){
    ranger = r - minr;
    rangeg = g - ming;
    rangeb = b - minb;
  }
#else
  ranger = r - minr;
  rangeg = g - ming;
  rangeb = b - minb;
#endif
}

void Emitter::SetShape(double shape0, double shape1){
  this->shape0 = abs(shape0);
  this->shape1 = abs(shape1);
  hasShape = true;
}

void Emitter::Start(){
  if (!emitting)
    emitting = true;
}

void Emitter::Stop(){
  if (emitting)
    emitting = false;
}

void Emitter::Update(double elapsed){
  //creación
  if (emitting){
    double rate = (rangerate) ? (WrapValue(rand(), rangerate) + minrate) * elapsed : minrate * elapsed;
    uint32 num = static_cast<uint32>(rate);
    emitionRemains += rate - num;
    while (emitionRemains >= 1.){
      num++;
      emitionRemains--;
    }
    double velx = 0.;
    double vely = 0.;
    double angularVel = 0.;
    double lifetime = 0.;
    uint8 cr = 0;
    uint8 cg = 0;
    uint8 cb = 0;
    for (uint32 i = 0; i < num; i++){
      velx = (rangevelx) ? WrapValue(rand(), rangevelx) + minvelx : minvelx;
      vely = (rangevely) ? WrapValue(rand(), rangevely) + minvely : minvely;
      angularVel = (rangeangvel) ? WrapValue(rand(), rangeangvel) + minangvel : minangvel;
      lifetime = (rangelifetime) ? WrapValue(rand(), rangelifetime) + minlifetime : minlifetime;
      Particle par(image, velx, vely, angularVel, lifetime, autofade);

      if (hasShape){
        double offsetX, offsetY;
        if (shape1){
          offsetX = WrapValue(rand(), shape0) - shape0 / 2.;
          offsetY = WrapValue(rand(), shape1) - shape1 / 2.;
        }
        else{
          offsetX = WrapValue(rand(), shape0 * 2.) - shape0;
          double rangeY = sqrt(shape0*shape0 - offsetX*offsetX);
          offsetY = (rangeY) ? WrapValue(rand(), rangeY * 2.) - rangeY : 0.;
        }
        par.SetPosition(x + offsetX, y + offsetY);
      }
      else
        par.SetPosition(x, y);

      cr = (ranger) ? rand() % ranger + minr : minr;
      cg = (rangeg) ? rand() % rangeg + ming : ming;
      cb = (rangeb) ? rand() % rangeb + minb : minb;
      par.SetColor(cr, cg, cb, 255);

      par.SetBlendMode(blendMode);
      particles.Add(par);
    }
  }

  //actualización
  for (uint32 i = 0; i < particles.Size(); i++)
    particles[i].Update(elapsed);

  //affectors
  for (uint32 i = 0; i < affectors.Size(); i++)
    for (uint32 j = 0; j < particles.Size(); j++)
    if (!particles[j].IsAffected())
      if (affectors[i].affect(&particles[j]))
        particles[j].SetAffected(true);

  //destrucción
  uint32 count = 0;
  for (uint32 i = 0; i < particles.Size(); i++)
    if (particles[i+count].GetLifetime() <= 0){
      particles.RemoveAt(i - count);
      count++;
    }
}

void Emitter::Render() const{
  for (uint32 i = 0; i < particles.Size(); i++)
    particles[i].Render();
}
