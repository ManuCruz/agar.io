#include "../include/filemanager.h"
#include "../include/pakfile.h"

FileManager* FileManager::mInstance = NULL;

FileManager& FileManager::Instance() {
  if ( !mInstance ) 
    mInstance = new FileManager();
  return *mInstance;
}

FileManager::~FileManager() {
  for ( uint32 i = 0; i < mPakFiles.Size(); i++ )
    delete mPakFiles[i];
}

bool FileManager::AddPak(const String& filename) {
  PakFile* pakFile = new PakFile(filename);
  if ( pakFile->IsValid() ) {
    mPakFiles.Add(pakFile);
    return true;
  } else {
    delete pakFile;
    return false;
  }
}

uint32 FileManager::FileSize(const String& filename) const {
  const PakFile* pakFile = PakForFile(filename);
  if ( pakFile )
    return pakFile->FileSize(filename);
  else {
    FILE* handle = fopen(filename.ToCString(), "rb");
    if ( handle ) {
      fseek(handle, 0, SEEK_END);
      uint32 size = ftell(handle);
      fclose(handle);
      return size;
    }
    return 0;
  }
}

bool FileManager::GetFile(const String& filename, void* buffer) const {
  const PakFile* pakFile = PakForFile(filename);
  if ( pakFile )
    return pakFile->GetFile(filename, buffer);
  else {
    FILE* handle = fopen(filename.ToCString(), "rb");
    if ( handle ) {
      fseek(handle, 0, SEEK_END);
      uint32 size = ftell(handle);
      fseek(handle, 0, SEEK_SET);
      fread(buffer, size, 1, handle);
      fclose(handle);
      return true;
    }
    return false;
  }
}

String FileManager::LoadString(const String& filename) const {
  uint32 fileSize = FileManager::Instance().FileSize(filename);
  if ( fileSize == 0 ) 
    return "";
  char* charBuffer = (char*)malloc(fileSize + 1);
  FileManager::Instance().GetFile(filename, charBuffer);
  charBuffer[fileSize] = 0;
  String buffer = charBuffer;
  free(charBuffer);
  return buffer;
}

const PakFile* FileManager::PakForFile(const String& filename) const {
  for ( int i = mPakFiles.Size() - 1; i >= 0; i-- )
    if ( mPakFiles[i]->FileExists(filename) )
      return mPakFiles[i];
  return NULL;
}
