#include "../include/font.h"
#include "../include/renderer.h"
#include "../include/math.h"
#include "../include/resourcemanager.h"
#include "../include/filemanager.h"
#include "../lib/stb_image.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "../lib/stb_truetype.h"

const uint32 NUM_CHARACTER = 256;

Font::Font(const String& filename){
  Image *image = ResourceManager::Instance().LoadImage(filename, 16, 16);

  fontSize = image->GetHeight();

  int32 width = 0;
  int32 height = 0;

  uint32 tam = FileManager::Instance().FileSize(filename);
  uint8 *buf = new uint8[tam];
  FileManager::Instance().GetFile(filename, buf);

  uint8 *buffer = stbi_load_from_memory(buf, tam, &width, &height, NULL, 4);

  delete buf;

  if (buffer) {
    uint32 tamBloque = height / 16;
    uint32 iterator;
    //para cada simbolo
    for (int32 i = 0; i < height; i += tamBloque)
      for (int32 j = 0; j < width; j += tamBloque){
        //para cada pixel de ese simbolo
        for (uint16 ii = 0; ii < tamBloque; ii++)
          for (uint16 jj = 0; jj < tamBloque; jj++){
          iterator = 4 * (width * (i + ii) + (j + jj));
          //si amarillo
          if (buffer[iterator] == 255 && buffer[iterator + 1] == 255 && buffer[iterator + 2] == 0){
            buffer[iterator + 3] = 0;

            Glyph glifo;
            glifo.setX(jj);
            glifo.setY(ii);
            glifos.Add(glifo);
          } //si rojo
          else if (buffer[iterator] == 255 && buffer[iterator + 1] == 0 && buffer[iterator + 2] == 0){
            buffer[iterator + 3] = 0;

            glifos.Last().setTamX(jj - glifos.Last().getX());
            glifos.Last().setTamY(ii - glifos.Last().getY());
          } //si no blanco
          else if (buffer[iterator] != 255 || buffer[iterator + 1] != 255 || buffer[iterator + 2] != 255)
            buffer[iterator + 3] = 0;
        }
      }

    //llamada para cargar la textura
    Renderer::Instance().TexImage2D(image->GetTexId(), buffer, static_cast<int16>(width), static_cast<int16>(height));

    images.Add(image);

    // liberación del buffer
    stbi_image_free(buffer);
  }
}

Font::Font(const String& filename, uint16 fontSize){
  this->fontSize = fontSize;

  if (filename.ExtractExt().Lower() == "ttf"){
    // cargar el fichero ttf en un buffer
    uint32 tam = FileManager::Instance().FileSize(filename);
    uint8 *buf = new uint8[tam];
    FileManager::Instance().GetFile(filename, buf);

    if (buf) {
      // recuperar la información contenida en el buffer
      stbtt_bakedchar charData[NUM_CHARACTER];
      bool imgPacked = false;
      uint32 imgsize = 128;
      uint8* alphaBuffer = new uint8[imgsize * imgsize];
      while (!imgPacked) {
        if (stbtt_BakeFontBitmap(buf, 0, fontSize, alphaBuffer, imgsize, imgsize, 0, NUM_CHARACTER, charData) > 0)
          imgPacked = true;
        else {
          delete[]alphaBuffer;
          imgsize *= 2;
          alphaBuffer = new uint8[imgsize * imgsize];
        }
      }
      delete[]buf;

      // convertir el alphaBuffer en colorBuffer
      uint8* colorBuffer = new uint8[imgsize * imgsize * 4];
      for (uint32 i = 0; i < imgsize*imgsize; i++) {
        colorBuffer[i * 4 + 0] = 255;
        colorBuffer[i * 4 + 1] = 255;
        colorBuffer[i * 4 + 2] = 255;
        colorBuffer[i * 4 + 3] = alphaBuffer[i];
      }
      delete[]alphaBuffer;

      for (uint32 i = 0; i < NUM_CHARACTER; i++){
        Glyph glifo;
        glifo.setX(charData[i].x0);
        glifo.setY(charData[i].y0);
        glifo.setTamX(charData[i].x1 - charData[i].x0);
        glifo.setTamY(charData[i].y1 - charData[i].y0);
        glifo.setOffsetY(charData[i].yoff);
        glifos.Add(glifo);

        //copiar el glifo en un buffer desde colorBuffer
        uint32 x4 = glifo.getX() * 4;
        uint32 tamX4 = glifo.getTamX() * 4;
        uint32 imgsize4 = imgsize * 4;

        uint8* auxBuffer = new uint8[tamX4 * glifo.getTamY()];

        for (int j = 0; j < glifo.getTamY(); j++)
          memcpy(&auxBuffer[tamX4 * j], &colorBuffer[imgsize4 * (j + glifo.getY()) + x4], tamX4);

        //crear una imagen a partir del buffer del glifo
        images.Add(new Image(auxBuffer, glifo.getTamX(), glifo.getTamY()));

        delete[]auxBuffer;
      }
      delete[]colorBuffer;
    }
  }
}

Font::~Font(){
  if (images.Size() > 1)
    for (uint32 i = 0; i < images.Size(); i++)
      delete images[i];
}

uint16 Font::GetSize() const{
  return fontSize;
}

uint32 Font::GetTextWidth(const String& text) const{
  if (glifos.Size() == 0)
    return images[0]->GetWidth() * text.Length();
  else{
    uint32 count = 0;
    for (int32 i = 0; i < text.Length(); i++){
      if (text[i] == 32)
        count += fontSize / 3;
      else
        count += glifos[text[i]].getTamX();
    }
    return count;
  }
}

uint32 Font::GetTextHeight(const String& text) const{
  if (glifos.Size() == 0)
    return images[0]->GetHeight();
  else{
    uint32 max = glifos[text[0]].getTamY();
    for (int32 i = 1; i < text.Length(); i++)
      max = (glifos[text[i]].getTamY() > max) ? glifos[text[i]].getTamY() : max;
    return max;
  }
}

float Font::GetTextSubHeight(const String& text) const{
  if (glifos.Size() == 0)
    return 0;
  else{
    float max = glifos[text[0]].getTamY() + glifos[text[0]].getOffsetY();
    for (int32 i = 1; i < text.Length(); i++)
      max = ((glifos[text[i]].getTamY() + glifos[text[i]].getOffsetY()) > max) ? glifos[text[i]].getTamY() + glifos[text[i]].getOffsetY() : max;
    return max;
  }
}

void Font::Render(const String& text, double x, double y) const{
  if (glifos.Size() == 0){ //fuente monoespaciada: no hay glifos
    uint16 offsetX = images[0]->GetWidth();
    for (int32 i = 0; i < text.Length(); i++)
      Renderer::Instance().DrawImage(images[0], x + (i * offsetX), y, static_cast<uint32>(text[i]));
  }
  else if (images.Size() == 1){ //fuente no monoespaciada: el array de imágenes tiene un único elemento
    uint16 offsetX = 0;
    for (int32 i = 0; i < text.Length(); i++){
      Renderer::Instance().DrawImage(images[0], x + offsetX - glifos[text[i]].getX(), y + glifos[text[i]].getY() - glifos[text[i]].getOffsetY(), static_cast<uint32>(text[i]));
      offsetX += glifos[text[i]].getTamX();
    }
  }
  else{ //fuente ttf: varios glifos y varias imagenes "virtuales"
    uint16 offsetX = 0;
    for (int32 i = 0; i < text.Length(); i++){
      if (text[i] == 32)
        offsetX += fontSize/3;
      else{
        Renderer::Instance().DrawImage(images[text[i]], x + offsetX, y + glifos[text[i]].getOffsetY());
        offsetX += glifos[text[i]].getTamX();
      }
    }
  }
}
