#include "../include/image.h"
#include "../include/glinclude.h"
#include "../include/math.h"
#include <math.h>
#include <stdlib.h>
#include "../include/renderer.h"
#include "../include/filemanager.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../lib/stb_image.h"

Image::Image() {
  filename = NULL;
  hframes = 1;
  vframes = 1;
  width = 0;
  height = 0;
  handlex = 0;
  handley = 0;
  gltex = 0;
  lastU = 1.;
  lastV = 1.;
}

Image::Image(const String &filename, uint16 hframes, uint16 vframes) {
  this->filename = filename;
  this->hframes = hframes;
  this->vframes = vframes;
  width = 0;
  height = 0;
  handlex = 0;
  handley = 0;
  gltex = 0;
  lastU = 1.;
  lastV = 1.;

  uint32 tam = FileManager::Instance().FileSize(filename);
  uint8 *buf = new uint8[tam];
  FileManager::Instance().GetFile(filename, buf);

  uint8 *buffer = stbi_load_from_memory(buf, tam, reinterpret_cast<int32*>(&width), reinterpret_cast<int32*>(&height), NULL, 4);

  delete buf;

 // Generamos la textura
  if ( buffer ) {
    uint16 newWidth = static_cast<uint16>(pow(2., ceil(Log2(width))));
    uint16 newHeight = static_cast<uint16>(pow(2., ceil(Log2(height))));
    if (width != newWidth || height != newHeight){
      uint8 *newBuffer = static_cast<uint8*>(calloc(newWidth * newHeight * 4, sizeof(uint8)));

      if (newBuffer){
        int32 totalWidthOriginal = width * 4;
        int32 totalWidthNew = newWidth * 4;

        //for (int i = 0; i < height; i++)
        //  for (int j = 0; j < totalWidthOriginal; j++)
        //    newBuffer[totalWidthNew*i + j] = buffer[totalWidthOriginal*i + j];

        for (int32 i = 0; i < height; i++)
          memcpy(&newBuffer[totalWidthNew*i], &buffer[totalWidthOriginal*i], totalWidthOriginal);

        lastU = static_cast<double>(width) / newWidth;
        lastV = static_cast<double>(height) / newHeight;

        gltex = Renderer::Instance().GenImage(newBuffer, newWidth, newHeight);

        free(newBuffer);
      }
    }
    else
      gltex = Renderer::Instance().GenImage(buffer, width, height);

    // liberación del buffer
    stbi_image_free(buffer);
  }
}

Image::Image(uint8* buffer, uint16 width, uint16 height) {
  filename = NULL;
  hframes = 1;
  vframes = 1;
  this->width = width;
  this->height = height;
  handlex = 0;
  handley = 0;
  gltex = Renderer::Instance().GenImage(buffer, width, height);
  lastU = 1.;
  lastV = 1.;
}

Image::~Image() {
  Renderer::Instance().DeleteImage(gltex);
}

void Image::Bind() const {
  Renderer::Instance().BindImage(gltex);
}
