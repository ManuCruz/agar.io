#include "../include/isometricsprite.h"
#include "../include/image.h"
#include "../include/map.h"
#include "../include/math.h"
#include <math.h>

IsometricSprite::IsometricSprite(Image* image) : Sprite(image){
  z = 0.;
  screenX = screenY = 0.;
  customColSize = false;
  customColW = customColH = 0;
}

void IsometricSprite::SetCollision(CollisionMode mode){
  mode == Sprite::COLLISION_PIXEL ? Sprite::SetCollision(Sprite::COLLISION_RECT) : Sprite::SetCollision(mode);
}

void IsometricSprite::SetCollisionSize(double colW, double colH){
  if (colW == 0. && colH == 0.)
    customColSize = false;
  else{
    customColW = colW;
    customColH = colH;
    customColSize = true;
  }
}

void IsometricSprite::Update(double elapsed, const Map* map){
  Sprite::Update(elapsed, map);
  TransformIsoCoords(GetX(), GetY(), z, &screenX, &screenY);
}

void IsometricSprite::UpdateCollisionBox(){
  double cx = GetX() - GetImage()->GetHandleX() * abs(GetScaleX());
  double cy = GetY() - GetImage()->GetHandleX() * abs(GetScaleX());
  double cw, ch;
  if (customColSize){
    cw = customColW * abs(GetScaleX());
    ch = customColH * abs(GetScaleX());
  }
  else{
    cw = GetImage()->GetWidth() * abs(GetScaleX());
    ch = GetImage()->GetWidth() * abs(GetScaleX());
  }

  Sprite::UpdateCollisionBox(cx, cy, cw, ch);
}