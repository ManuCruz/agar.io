#include "../include/localizationmanager.h"
#include "../include/filemanager.h"
#include "../lib/rapidxml.hpp"

using namespace rapidxml;

LocalizationManager* LocalizationManager::instance = NULL;

LocalizationManager& LocalizationManager::Instance() {
  if ( !instance ) instance = new LocalizationManager();
  return *instance;
}

bool LocalizationManager::ParseLanguage(const String& filename) {
  // Cargamos string
  String buffer = FileManager::Instance().LoadString(filename);

  // Analizamos XML
  xml_document<> doc;
  doc.parse<0>((char*)buffer.ToCString());

  // Obtenemos informacion de cabecera
  xml_node<>* langNode = doc.first_node("language");
  String langName = langNode->first_attribute("name")->value();
  LangDef langDef(langName);

  // Obtenemos todas las claves
  xml_node<>* stringNode = langNode->first_node("string");
  if (!stringNode)
    return false;
  while ( stringNode ) {
    String key = stringNode->first_attribute("key")->value();
    String value = stringNode->first_attribute("value")->value();
    langDef.AddString(key, value);
    stringNode = stringNode->next_sibling("string");
  }

  // Guardamos el lenguaje
  mLanguages.Add(langDef);

  return true;
}
